from django import forms
from AppOne.models import Student, School, UserProfile
from django.contrib.auth.models import User


class Contact_us_form(forms.Form):
    email = forms.EmailField()
    phone_number = forms.IntegerField()
    query = forms.CharField(max_length=350)


class Student_form(forms.ModelForm):
    class Meta:
        model = Student
        fields = '__all__'


class SchoolForm(forms.ModelForm):
    class Meta:
        model = School
        fields = '__all__'


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ['user']
        # fields = '__all__'


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        # fields = '__all__'
        fields = ('first_name', 'last_name', 'username', 'password', 'email')


class LoginForm(forms.Form):
    username = forms.CharField(max_length=554)
    password = forms.CharField(widget=forms.PasswordInput)