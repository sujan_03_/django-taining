from rest_framework import serializers
from AppOne.models import School, Student


class SchoolSerializer(serializers.Serializer):
    name = serializers.CharField()
    student_count = serializers.IntegerField()

    def create(self, validated_data):
        return School.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.student_count = validated_data.get('student_count', instance.student_count)
        instance.save()
        return instance


class StudentSerializer(serializers.ModelSerializer):
    school = SchoolSerializer()
    class Meta:
        model = Student
        fields = '__all__'

class StudentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'
