from django.contrib.auth.models import User
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_delete, post_save
# Create your models here.


class School(models.Model):
    name = models.CharField(max_length=50)
    student_count = models.IntegerField(null=False)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=90)
    age = models.IntegerField(default=10,null=False)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15,null=False)
    height = models.CharField(max_length=5,default='5.9')
    school = models.ForeignKey(School,on_delete=models.CASCADE,null=True)
    photo = models.ImageField(upload_to="profile_pics", null=True)


class UserProfile(models.Model):
    photo = models.ImageField(upload_to="user_photo")
    cv = models.FileField(upload_to="user_cv")
    phone_no = models.CharField(max_length=13)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

class Items(models.Model):
    name = models.CharField(max_length=242, default="Buffer")
    quantity = models.IntegerField()


class Transaction(models.Model):
    item = models.ForeignKey(Items, on_delete=models.CASCADE)
    quantity_sold = models.IntegerField()


@receiver(post_save, sender=Transaction, dispatch_uid="update_items")
def update_items(sender, instance, **kwargs):
    instance.item.quantity -=instance.quantity_sold
    instance.item.save()

@receiver(post_delete, sender=Transaction, dispatch_uid="update_items_delete")
def increase_items(sender, instance, **kwargs):
    instance.item.quantity +=instance.quantity_sold
    instance.item.save()