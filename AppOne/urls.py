from django.urls import path
from AppOne import views
# from AppOne.views import SchoolList

app_name = 'AppOne'

urlpatterns = [
    path('index/', views.index,name="app1_index"),
    path('student_list/', views.studentlist, name="student_list"),
    path('school_list/', views.schoollist, name="school_list"),
    path('contact_us/', views.contact, name="contact"),
    path('add_student/', views.add_student, name="add_student"),
    path('delete_student/<int:id>/', views.delete_student, name="delete_student"),
    path('edit_student/<int:id>/', views.edit_student, name="edit_student"),
    path('edit_school/<int:id>/', views.edit_school, name="edit_school"),
    path('user_register/', views.user_register, name="userRegister"),
    path('user_login/', views.user_login, name="userLogin"),
    path('user_logout/', views.user_logout, name="userLogout"),
    path('user_verify/', views.verify_user, name="verify_user"),
    path('api/school/', views.school_api),
    path('api/school/<int:pk>/', views.school_detail),
    path('api/student/', views.student_api),
    path('api/schools/', views.SchoolList.as_view()),
    path('api/school/<int:pk>', views.SchoolDetail.as_view()),

]