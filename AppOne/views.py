from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse, Http404
from django.shortcuts import render, HttpResponse, redirect
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework import status
from AppOne.models import Student
from AppOne.models import School
from AppOne.forms import Contact_us_form, Student_form, SchoolForm, UserForm, UserProfileForm, LoginForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
from AppOne.serializers import SchoolSerializer, StudentSerializer, StudentCreateSerializer


def index(request):
    # return HttpResponse('<h1>this is index of app one</h1>')
    return render(request, 'AppOne/index.html', context={"test_key": ["test1", "test2"]})


def login1(request):
    return HttpResponse('<h1>this is login of app one</h1>')


@login_required(login_url='/AppOne/user_login')
def studentlist(request):
    students = Student.objects.all()
    msgs = messages.get_messages(request)
    return render(request, 'AppOne/student_list.html',context={"students":students, "messages":msgs})


def schoollist(request):
    msgs = messages.get_messages(request)
    school = School.objects.all()
    return render(request, 'AppOne/school_list.html', context={"schools":school, "messages":msgs})

def contact(request):
    if request.method == 'GET':
        contact = Contact_us_form()
        return render(request, 'AppOne/contact_us_form.html', context={'contact_us_form':contact})
    else:
        contact_us_form = Contact_us_form(request.POST)
        if contact_us_form.is_valid():
            print("Email:", contact_us_form.cleaned_data['email'])
            print("Phone no:", contact_us_form.cleaned_data['phone_number'])
            print("Query:", contact_us_form.cleaned_data['query'])
            return HttpResponse("Submitted")
        else:
            return HttpResponse("error")


def add_student(request):
    if request.method == 'GET':
        student = Student_form()
        return render(request, 'AppOne/add_student.html', context={'form':student})
    else:
        student = Student_form(request.POST, request.FILES)
        if student.is_valid():
            student.save()
            return HttpResponse('student saved successfully')
        else:
            return HttpResponse('error')


def delete_student(request,id):
    st = Student.objects.get(id=id)
    st.delete()
    return HttpResponse("Student Deleted")

def edit_student(request, id):
    student = Student.objects.get(id=id)
    if request.method == "GET":
        st_form = Student_form(instance=student)
        return render(request, 'AppOne/edit_student.html',context={'form':st_form})
    else:
        st_form = Student_form(request.POST, request.FILES, instance=student)
        if st_form.is_valid():
            st_form.save()
            # student_list = Student.objects.all()
            # return render(request, 'AppOne')
            messages.success(request,"Your changes are saved")
            return redirect('student_list')


def edit_school(request, id):
    school = School.objects.get(id=id)
    if request.method == "GET":
        sc_form = SchoolForm(instance=school)
        return render(request, 'AppOne/edit_school.html', context={'form': sc_form})
    else:
        sc_form = SchoolForm(request.POST, instance=school)
        if sc_form.is_valid():
            sc_form.save()
            messages.success(request, "Your changes are saved")
            return redirect('school_list')


def user_register(request):
    if request.method == 'GET':
        user_form = UserForm()
        user_profile_form = UserProfileForm()
        return render(request, 'AppOne/user_registration.html', context={"user_form": user_form, "user_profile_form": user_profile_form})
    else:
        user = UserForm(request.POST)
        user = user.save()
        user.set_password(user.password)
        user.save()
        user_profile = UserProfileForm(request.POST, request.FILES)
        user_prof = user_profile.save(commit=False)
        user_prof.user = user
        user_prof.save()
        return HttpResponse(1)


def user_login(request):
    if request.method == "GET":
        login = LoginForm()
        return render(request, 'AppOne/login.html', context={"loginForm": login})


def user_logout(request):
    logout(request)
    messages.success(request, "User Logged-out !!!")
    return redirect('AppOne:userLogin')


def verify_user(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                messages.success(request, 'Login Successful')

                return redirect('AppOne:add_student')
            else:
                messages.warning(request, "Inactive User")
                return redirect('AppOne:userLogin')
        else:
            messages.warning(request, "User not found! error")
            return redirect('AppOne:userLogin')

@csrf_exempt
def school_api(request):
    if request.method == 'GET':
        school_all = School.objects.all()
        school_serialized = SchoolSerializer(school_all, many=True)
        return JsonResponse(school_serialized.data, safe=False)
    else:
        data =JSONParser().parse(request)
        print(data)
        serializer = SchoolSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def school_detail(request, pk):
    try:
        school = School.objects.get(pk=pk)
    except School.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = SchoolSerializer(school)
        return JsonResponse(serializer.data)
    elif request.method == 'DELETE':
        school.delete()
        return HttpResponse(status=204)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = SchoolSerializer(school, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors,status=400)
    else:
        return HttpResponse(status=500)


@csrf_exempt
def student_api(request):
    if request.method == 'GET':
        student = Student.objects.all()
        student_serialized = StudentSerializer(student, many=True)
        return JsonResponse(student_serialized.data, safe=False)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        student_serialized = StudentCreateSerializer(data=data)
        if student_serialized.is_valid():
            student_serialized.save()
            return JsonResponse(student_serialized.data, status=201)
        return HttpResponse(status=402)


class SchoolList(APIView):
    def get(self,request):
        school = School.objects.all()
        serializer = SchoolSerializer(school,many=True)
        return JsonResponse(serializer.data, safe=False)
    def post(self,request):
        serializer =SchoolSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data,status=201)
        return JsonResponse(serializer.errors, status=400)


class SchoolDetail(APIView):
    def get_object(self, pk):
        try:
            return School.objects.get(pk=pk)
        except School.DoesNotExist:
            raise Http404

    def get(self,request, pk):
        school = self.get_object(pk)
        serializer = SchoolSerializer(school)
        return JsonResponse(serializer.data)

    def put(self, request, pk):
        school = self.get_object(pk)
        serializer = SchoolSerializer(school, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
