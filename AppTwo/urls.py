from django.urls import path
from AppTwo import views
urlpatterns = [
    path('index/',views.index,name="app2_index"),
    path('login/',views.login)
]