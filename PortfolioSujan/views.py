from django.shortcuts import render


# Create your views here.
def home(request):
    return render(request, 'PortfolioSujan/home.html')


def education(request):
    return render(request, 'PortfolioSujan/education.html')


def photos(request):
    return render(request, 'PortfolioSujan/photos.html')


def contact(request):
    return render(request, 'PortfolioSujan/contact.html')
