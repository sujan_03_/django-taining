from django.urls import path
from PortfolioSujan import views
urlpatterns = [
    path('home/', views.home, name="portfolio_sujan_home"),
    path('education/', views.education, name="portfolio_sujan_education"),
    path('photos/', views.photos, name="portfolio_sujan_photos"),
    path('contact/', views.contact, name="portfolio_sujan_contact"),
]